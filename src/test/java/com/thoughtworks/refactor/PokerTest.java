package com.thoughtworks.refactor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PokerTest {
    @Test
    void test_compare_Straight_Flush_with_Four_Of_A_Kind_card_type() {
        String black = "A@ K@ Q@ J@ T@";
        String white = "9! 9@ 9# 9$ 5#";

        String actualResult = new Poker().compairResult(black, white);

        assertEquals("black wins - StraightFlush", actualResult);
    }

    @Test
    void test_compare_both_Straight_Flush() {
        String black = "A@ K@ Q@ J@ T@";
        String white = "K! Q! J! T! 9!";

        String actualResult = new Poker().compairResult(black, white);

        assertEquals("black wins - high card:A", actualResult);
    }
}
